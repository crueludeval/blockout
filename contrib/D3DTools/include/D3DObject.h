//***************************************************
// Class D3DObject
// Class for D3DObject (DirectX 8.0)
// Author: JL Pons (2002)
//***************************************************
//#define WITHODE 1

#ifndef _D3DOBJECTH_
#define _D3DOBJECTH_

#include <stdio.h>
#include <math.h>
#include <D3DX8.h>
#ifdef WITHODE
#include <ode/ode.h>
#endif
#include "D3DTexture.h"
#include "D3DTypes.h"

class D3DObject {

public:

  // Contructor
  D3DObject();

  // Restore device object
  HRESULT RestoreDeviceObjects();

  // Invalidate device object
  HRESULT InvalidateDeviceObjects();

  // Creation from a D3D file
  HRESULT CreateFromFile(LPDIRECT3DDEVICE8 pd3dDevice,char *filename);

  // 3D sprite creation
  HRESULT CreateSprite(LPDIRECT3DDEVICE8 pd3dDevice,
    char *diff_name,
    char *alpha_name,
    float width,
    float height,
    D3DXVECTOR3 pos );

  // 2D sprite creation (Screen coordinates sprites)
  HRESULT Create2DSprite(LPDIRECT3DDEVICE8 pd3dDevice,
    char *diff_name,
    char *alpha_name,
    int  x1,int y1,
    int  x2,int y2);

  // Create a set of particle (3D Sprite) particle
	HRESULT CreateParticles(LPDIRECT3DDEVICE8 pd3dDevice,
							   char       *diff_name,
							   char       *alpha_name,
							   D3DXVECTOR3 orig,
							   float       fx_length,
							   float       particle_size,
							   int         particle_number );

  // Animate particle
	HRESULT SetParticleOffset(int i,float offset);

  // Update 3D sprite coordinates to turn them
  // in front of the camera defined by vAt,vDir and vUp.
  HRESULT UpdateSprite(D3DXVECTOR3 vAt,D3DXVECTOR3 vDir,D3DXVECTOR3 vUp);
  HRESULT UpdateSprite(D3DXVECTOR3 vAt,D3DXVECTOR3 vDir,D3DXVECTOR3 vUp,
    float nWidth,float nHeight);
  HRESULT UpdateSprite(D3DXVECTOR3 vAt,D3DXVECTOR3 vDir,D3DXVECTOR3 vUp,
    float nWidth,float nHeight,
    float x1,float y1,float x2,float y2);

  // Update 2D sprite coordinates (screen coordinates)
  HRESULT UpdateSprite(int x1,int y1,int x2,int y2);
  HRESULT UpdateSprite(int x1,int y1,int x2,int y2,
                       float mx1,float my1,float mx2,float my2);

  // Set sprite mapping (2D and 3D)
  HRESULT SetSpriteMapping(float x1,float y1,float x2,float y2);

  // Set sprite color (2D Only)
  // Default if 255,255,255
  HRESULT SetSpriteColor(DWORD color);

  // Sort objet face (This can help to perform some alpha FX)
  HRESULT SortFace(D3DXMATRIX *matPos , D3DXVECTOR3 *vEye);

  // Overrides all texture of this objet
  HRESULT SetTexture(D3DTexture *map);

  //Apply mapping coordinates translation
  HRESULT TranslateMapping(float tu,float tv);
  HRESULT ScaleMapping(float ru,float rv);

  //Set global transparency [0=Transparent 1=Opaque]
  HRESULT SetTransparency(float t);

  //Set black transparency for the whole object
  //Pixel are transparent when aplha channel is zero
  HRESULT SetBlackTransparent(BOOL t);

  //Make this object glow object
  //This override alpha settings
  HRESULT SetGlow(BOOL t);

  //Set 2sided [0=Normal(CULL_CW) 1=2sided]
  HRESULT SetTwoSided(BOOL b);

  // Explode the object into triangle
  HRESULT Explode(float distance);

  // Adjust geometry to be able to perform explosion
  HRESULT PrepareForExplosion(LPDIRECT3DDEVICE8 pd3dDevice);

  // Render the object on the device
  HRESULT Render(LPDIRECT3DDEVICE8 pd3dDevice);

  // Return the number of polygon
  DWORD GetNbPoly();

  // Return the bounding box of the object
  void GetDimension(D3DXVECTOR3 *dim);

  // Translate the object
  HRESULT Translate(float x,float y,float z);

  // Set a blended material to this object.
  // The material result in a blending of
  // the original material plus the given 
  // transparent material. 
  // (Only the first object is affected)
  // uDScale and uDScale controls the diffuse mapping tile
  // uAScale and uAScale controls the transparency mapping tile
  HRESULT SetBlendMaterial(LPDIRECT3DDEVICE8 pd3dDevice,
    char *diff_name, float uDScale,float vDScale,
    char *alpha_name, float uAScale,float vAScale
  );

#ifdef WITHODE
  // Return an ODE geom object (_3DOBJ only)
  HRESULT GetODEGeom(dSpaceID space,dGeomID *id);
#endif

private:

  void CreateObjectArray(int nb_obj);

  _3DObject *obj;
  DWORD     nb_obj;
  BOOL	    load_ok;
  float     m_Transparency;
  BOOL      explose;
  BOOL      m_2Sided;
  BOOL      m_BlackTrans;
  BOOL      m_Glow;
  D3DTexture	*blendamap;  // Handle to the texture (for blending)
  D3DTexture  *blendmap;   // Handle to the texture (for blending)
#ifdef WITHODE
  dVector3    *odePts;     // Used by ODE
  int         *odeIdx;     // Used by ODE
#endif

};

#endif /* _D3DOBJECTH_ */
