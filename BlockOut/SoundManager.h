/*
 	File:        SoundManager.h
  Description: Sound management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include <dmutil.h>

#ifndef SOUNDMANAGERH
#define SOUNDMANAGERH

class SoundManager {

  public:
    SoundManager();

    // Initialise the sound manager
    HRESULT Create(HWND hwnd);

    // Play sounds
    void PlayBlub();
    void PlayWozz();
    void PlayTchh();

    // Game sounds
    void PlayLine();
    void PlayLevel();
    void PlayEmpty();
    void PlayWellDone();
    void PlayLine2();
    void PlayLevel2();
    void PlayEmpty2();
    void PlayWellDone2();
    void PlayHit();

    // Demo music
    void PlayMusic();
    void StopMusic();

    // Get error message
    char *GetErrorMsg();

    // Enable/Disable sound
    void SetEnable(BOOL enable);

  private:
    HRESULT InitSound(char *fileName,CMusicSegment** sound);

    char errMsg[1024];
    BOOL enabled;

    CMusicManager*       musicManager;
    CMusicSegment*       blubSound;
    CMusicSegment*       wozzSound;
    CMusicSegment*       tchhSound;
    CMusicSegment*       lineSound;
    CMusicSegment*       levelSound;
    CMusicSegment*       wellDoneSound;
    CMusicSegment*       emptySound;
    CMusicSegment*       line2Sound;
    CMusicSegment*       level2Sound;
    CMusicSegment*       wellDone2Sound;
    CMusicSegment*       empty2Sound;
    CMusicSegment*       creditsMusic;
    CMusicSegment*       hitSound;

};

#endif /* SOUNDMANAGERH */