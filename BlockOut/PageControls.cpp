/*
 	File:        PageControls.cpp
  Description: Controls menu page
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Menu.h"

// -----------------------------------------------------------

void PageControls::Prepare(int iParam,void *pParam) {
  nbItem  = 8;
  selItem = 2;
  startTime = 0.0f;
  rotPos = 0;
  editMode = FALSE;
  cursorVisible = FALSE;
}

// -----------------------------------------------------------

void PageControls::Render(LPDIRECT3DDEVICE8 pd3dDevice) {

  pitBack.Render(pd3dDevice);

  mParent->RenderTitle("GAME CONTROLS");

  RenderKey(4,5,mParent->GetSetup()->GetKRx1(),2);
  RenderKey(4,6,mParent->GetSetup()->GetKRx2(),3);
  RenderKey(16,5,mParent->GetSetup()->GetKRy1(),4);
  RenderKey(16,6,mParent->GetSetup()->GetKRy2(),5);
  RenderKey(27,5,mParent->GetSetup()->GetKRz1(),6);
  RenderKey(27,6,mParent->GetSetup()->GetKRz2(),7);

  if( rotMode ) {
    mParent->RenderText(2,6,FALSE,"-");
    mParent->RenderText(31,6,FALSE,"-");
  } else {
    mParent->RenderText(2,5,FALSE,"-");
    mParent->RenderText(31,5,FALSE,"-");
  }

  mParent->RenderText(0,7,FALSE,"Auto:");
  mParent->RenderText(9,7,(selItem==0),"[QWERTY]");
  mParent->RenderText(18,7,(selItem==1),"[AZERTY]");

  if( !editMode )
    mParent->RenderText(0,9,FALSE,"[Cursor]Move [Space]Drop [P]Pause");
  else
    mParent->RenderText(0,9,FALSE,"Press new key or [Esc] to cancel");

  // Render sample blocks
  pd3dDevice->SetViewport(&blockView);
  pd3dDevice->SetTransform(D3DTS_PROJECTION,&blockProj);
  for(int i=0;i<3;i++) {
    pd3dDevice->SetTransform( D3DTS_WORLD, matBlock + i );
    BOOL redMode = editMode && ((!rotMode && (selItem==2*i+2)) || (rotMode && (selItem==2*i+3)));
    block[i].Render(pd3dDevice,redMode,FALSE);
  }

}

// -----------------------------------------------------------

void PageControls::RenderKey(int x,int y,char k,int pos) {

  char tmp[256];

  if( !editMode || selItem!=pos ) {
    sprintf(tmp,"[%c]",k);
    mParent->RenderText(x,y,(selItem==pos),tmp);
  } else {
    mParent->RenderText(x,y,FALSE,"[");
    mParent->RenderText(x+1,y,cursorVisible," ");
    mParent->RenderText(x+2,y,FALSE,"]");
  }

}

// -----------------------------------------------------------

void PageControls::ProcessAnimation(float fTime) {

  // Process block animation
  if( startTime == 0.0f )
    startTime = fTime;

  D3DXMATRIX matT;
  D3DXMATRIX matR;

  float aTime = fTime-startTime;
  float startAngle = (float)rotPos * PI/2.0f;
  float angle = startAngle + PI*aTime;

  if( aTime >= 0.5f && aTime <= 0.75 ) {

    // Make a short pause
    angle = startAngle + PI/2.0f;

  } else if( aTime > 0.75f ) {

    startTime = fTime;
    rotPos++;
    if( rotPos >= 4 )
      rotPos = 0;
    angle = startAngle + PI/2.0f;

  }

  if( rotMode ) {

    //ASD
    D3DXMatrixRotationX(&matR,angle);
    Place(block + 0,matBlock + 0,&matR,6.0f, 1.0f,1.0f);

    D3DXMatrixRotationY(&matR,-angle);
    Place(block + 1,matBlock + 1,&matR,2.0f, 0.0f,1.0f);

    D3DXMatrixRotationZ(&matR,angle);
    Place(block + 2,matBlock + 2,&matR,0.0f, 1.0f,1.0f);

  } else {

    // QWE
    D3DXMatrixRotationX(&matR,-angle);
    Place(block + 0,matBlock + 0,&matR,6.0f, 1.0f,1.0f);

    D3DXMatrixRotationY(&matR,angle);
    Place(block + 1,matBlock + 1,&matR,2.0f, 0.0f,1.0f);

    D3DXMatrixRotationZ(&matR,-angle);
    Place(block + 2,matBlock + 2,&matR,0.0f, 1.0f,1.0f);

  }


}

// -----------------------------------------------------------

void PageControls::ProcessEdit(BYTE *keys) {

  if( keys[VK_ESCAPE] ) {
    editMode = false;
    keys[VK_ESCAPE] = 0;
  }

  // Search key
  BOOL found = FALSE;
  BYTE i = 'A';
  while( i<='Z' && !found ) {
    found = ( keys[i]!=0 );
    if(!found) i++;
  }

  if( found ) {
    switch(selItem) {
      case 2: // Q
        mParent->GetSetup()->SetKRx1(i);
      break;
      case 4: // W
        mParent->GetSetup()->SetKRy1(i);
      break;
      case 6: // E
        mParent->GetSetup()->SetKRz1(i);
      break;
      case 3: // A
        mParent->GetSetup()->SetKRx2(i);
      break;
      case 5: // Z
        mParent->GetSetup()->SetKRy2(i);
      break;
      case 7:  // D
        mParent->GetSetup()->SetKRz2(i);
      break;
    }
    editMode = FALSE;
    keys[i] = 0;
  }

}

// -----------------------------------------------------------

void PageControls::ProcessMenuDis(BYTE *keys) {

  int dir = 0;
  if( (dir==0) && keys[VK_LEFT] ) dir = 1;
  if( (dir==0) && keys[VK_RIGHT]) dir = 2;
  if( (dir==0) && keys[VK_UP]   ) dir = 3;
  if( (dir==0) && keys[VK_DOWN] ) dir = 4;

  // Menu displacement
  switch(selItem) {
    case 0:
      switch(dir) {
        case 2:
          selItem = 1;
          break;
        case 3:
          selItem = 3;
          break;
      }
    break;
    case 1:
      switch(dir) {
        case 1:
          selItem = 0;
          break;
        case 3:
          selItem = 7;
          break;
      }
    break;
    case 2:
      switch(dir) {
        case 2:
          selItem = 4;
          break;
        case 4:
          selItem = 3;
          break;
      }
    break;
    case 3:
      switch(dir) {
        case 2:
          selItem = 5;
          break;
        case 3:
          selItem = 2;
          break;
        case 4:
          selItem = 0;
          break;
      }
    break;
    case 4:
      switch(dir) {
        case 1:
          selItem = 2;
          break;
        case 2:
          selItem = 6;
          break;
        case 4:
          selItem = 5;
          break;
      }
    break;
    case 5:
      switch(dir) {
        case 1:
          selItem = 3;
          break;
        case 2:
          selItem = 7;
          break;
        case 3:
          selItem = 4;
          break;
        case 4:
          selItem = 0;
          break;
      }
    break;
    case 6:
      switch(dir) {
        case 1:
          selItem = 4;
          break;
        case 4:
          selItem = 7;
          break;
      }
    break;
    case 7:
      switch(dir) {
        case 1:
          selItem = 5;
          break;
        case 3:
          selItem = 6;
          break;
        case 4:
          selItem = 1;
          break;
      }
    break;
  }

}

// -----------------------------------------------------------

int PageControls::Process(BYTE *keys,float fTime) {

  if( !editMode )
    ProcessMenuDis(keys);
  else
    ProcessEdit(keys);

  // Reset cursor key
  keys[VK_LEFT] = 0;
  keys[VK_RIGHT] = 0;
  keys[VK_UP] = 0;
  keys[VK_DOWN] = 0;

  if( keys[VK_RETURN] ) {
    switch(selItem) {
      case 0: // Reset to Qwerty
        mParent->GetSetup()->ResetToQwerty();
        break;
      case 1: // Reset to Azerty
        mParent->GetSetup()->ResetToAzerty();
        break;
      case 2: // Configure Q
      case 4: // Configure W
      case 6: // Configure E
      case 3: // Configure A
      case 5: // Configure S
      case 7: // Configure D
        for(int i='A';i<='Z';i++) keys[i] = 0;
        editMode = TRUE;
        startEditTime = fTime;
        break;
    }
    keys[VK_RETURN] = 0;
  }

  if( keys[VK_ESCAPE] ) {
    mParent->ToPage(&mParent->optionsPage);
    keys[VK_ESCAPE] = 0;
  }

  // rotation mode
  rotMode = (selItem==3) ||  (selItem==5) || (selItem==7);

  // Cursor
  if( editMode )
    cursorVisible = ( (fround((startEditTime - fTime) * 2.0f)%2) == 0 );
  else
    cursorVisible = FALSE;

  ProcessAnimation(fTime);

  return 0;
}

// -----------------------------------------------------------

void PageControls::Place(PolyCube *obj,D3DXMATRIX *mat,D3DXMATRIX *matR,float x,float y,float z) {

  D3DXMATRIX matT;
  D3DXMATRIX matT1;
  D3DXMATRIX matT2;

  D3DXVECTOR3 center = obj->GetRCenter();
  D3DXMatrixTranslation(&matT1,-center.x,-center.y,-center.z);
  D3DXMatrixTranslation(&matT2,center.x,center.y,center.z);

  D3DXMatrixMultiply(mat,&matT1,matR);
  D3DXMatrixMultiply(mat,mat,&matT2);
  x = (x * cubeSide);
  y = (y * cubeSide);
  z = (z * cubeSide);
  D3DXMatrixTranslation(&matT,x,y,z);
  D3DXMatrixMultiply(mat,mat,&matT);

}

// -----------------------------------------------------------

HRESULT PageControls::Create(LPDIRECT3DDEVICE8 pd3dDevice) {

  HRESULT hr;

  // Get back buffer dimemsion
  D3DSURFACE_DESC desc;
  LPDIRECT3DSURFACE8 pBackBuffer;
  pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
  pBackBuffer->GetDesc( &desc );
  pBackBuffer->Release();

  // Create pit background
  float pitX = (float)desc.Width  * 0.2187f;
  float pitY = (float)desc.Height * 0.485f;
  float pitW = (float)desc.Width  * 0.5625f;
  float pitH = (float)desc.Height * 0.25f;

  hr = pitBack.Create2DSprite(pd3dDevice,"images\\menupit.png","none",0,0,0,0);
  pitBack.UpdateSprite(fround(pitX),fround(pitY),
                       fround(pitX+pitW),fround(pitY+pitH),
                       0.0f,0.0f,1.0f,0.337f);
  if(FAILED(hr)) return hr;

  // Emulate pit 9*3*12
  cubeSide = 1.0f/9.0f;
  VERTEX org = v(-0.5f,-1.5f*cubeSide,STARTZ);

  block[0].AddCube(0,0,0);
  block[0].AddCube(1,0,0);
  block[0].AddCube(2,0,0);
  block[0].AddCube(2,1,0);
  hr = block[0].Create(pd3dDevice,cubeSide,org,FALSE);
  if(FAILED(hr)) return hr;

  block[1].AddCube(1,0,0);
  block[1].AddCube(2,0,0);
  block[1].AddCube(1,2,0);
  block[1].AddCube(1,1,0);
  hr = block[1].Create(pd3dDevice,cubeSide,org,FALSE);
  if(FAILED(hr)) return hr;

  block[2].AddCube(0,0,0);
  block[2].AddCube(1,0,0);
  block[2].AddCube(1,1,0);
  hr = block[2].Create(pd3dDevice,cubeSide,org,FALSE);
  if(FAILED(hr)) return hr;

  for(int i=0;i<3;i++)
    D3DXMatrixIdentity(matBlock+i);

  // block viewport 
  // Build a square viewport, pit background image has an aspect ratio of 3
  int vY = fround( pitY - pitH );
  int vH = fround( 3.0f * pitH );
  
  if( vH+vY > (int)desc.Height ) {
    // Should not happen
    vH = desc.Height - vY;
  }

  blockView.X      = fround(pitX);
  blockView.Y      = vY;
  blockView.Width  = fround(pitW);
  blockView.Height = vH;
  blockView.MinZ   = 0.0f;
  blockView.MaxZ   = 1.0f;

  // Projection matrix
  FLOAT fAspectRatio = 1.0f;
  D3DXMatrixPerspectiveFovRH( &blockProj, D3DXToRadian(60.0f), fAspectRatio, 0.1f, FAR_DISTANCE );

  return S_OK;

}

// -----------------------------------------------------------

void PageControls::InvalidateDeviceObjects() {

  pitBack.InvalidateDeviceObjects();
  for(int i=0;i<3;i++)
    block[i].InvalidateDeviceObjects();

}

