/*
 	File:        SoundManager.cpp
  Description: Sound management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "SoundManager.h"
#include <stdio.h>

// ------------------------------------------------

SoundManager::SoundManager() {
  enabled = FALSE;
  musicManager = NULL;
  blubSound = NULL;
  wozzSound = NULL;
  tchhSound = NULL;
  lineSound = NULL;
  levelSound = NULL;
  wellDoneSound = NULL;
  emptySound = NULL;
  line2Sound = NULL;
  level2Sound = NULL;
  wellDone2Sound = NULL;
  empty2Sound = NULL;
  creditsMusic = NULL;
  hitSound = NULL;
}

// ------------------------------------------------

HRESULT SoundManager::Create(HWND hwnd) {

  HRESULT hr;
  strcpy(errMsg,"");

  musicManager = new CMusicManager();
  hr = musicManager->Initialize(hwnd);
  if( FAILED(hr) ) {
    sprintf(errMsg,"General error");
    delete musicManager;
    musicManager = NULL;
    return hr;
  }

  hr = InitSound("sounds\\blub.wav",&blubSound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\wozz.wav",&wozzSound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\tchh.wav",&tchhSound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\line.wav",&lineSound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\level.wav",&levelSound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\empty.wav",&emptySound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\welldone.wav",&wellDoneSound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\line2.wav",&line2Sound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\level2.wav",&level2Sound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\empty2.wav",&empty2Sound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\welldone2.wav",&wellDone2Sound);
  if(FAILED(hr)) return hr;
  hr = InitSound("sounds\\hit.wav",&hitSound);
  if(FAILED(hr)) return hr;

  return S_OK;

}

// ------------------------------------------------

void SoundManager::SetEnable(BOOL enable) {
  enabled = enable;
}

// ------------------------------------------------

HRESULT SoundManager::InitSound(char *fileName,CMusicSegment** sound) {

  HRESULT hr;

  hr = musicManager->CreateSegmentFromFile(sound,fileName,TRUE,FALSE);
  if(FAILED(hr)) {
    sprintf(errMsg,"Error while loading %s",fileName);
    delete musicManager;
    musicManager = NULL;
    return hr;
  }

  return S_OK;

}

// ------------------------------------------------

char *SoundManager::GetErrorMsg() {

  return errMsg;

}

// ------------------------------------------------

void SoundManager::PlayBlub() {
  if( enabled && musicManager ) blubSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayWozz() {
  if( enabled && musicManager ) wozzSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayTchh() {
  if( enabled && musicManager ) tchhSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayLine() {
  if( enabled && musicManager ) lineSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayLevel() {
  if( enabled && musicManager ) levelSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayEmpty() {
  if( enabled && musicManager ) emptySound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayWellDone() {
  if( enabled && musicManager ) wellDoneSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayLine2() {
  if( enabled && musicManager ) line2Sound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayLevel2() {
  if( enabled && musicManager ) level2Sound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayEmpty2() {
  if( enabled && musicManager ) empty2Sound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayWellDone2() {
  if( enabled && musicManager ) wellDone2Sound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayHit() {
  if( enabled && musicManager ) hitSound->Play( DMUS_SEGF_SECONDARY );  
}
void SoundManager::PlayMusic() {
  if( musicManager ) {
    if( creditsMusic==NULL ) {
      HRESULT hr = InitSound("sounds\\music.wav",&creditsMusic);
      if( FAILED(hr) ) creditsMusic = NULL;
    }
    if( creditsMusic ) {
      creditsMusic->SetRepeats(DMUS_SEG_REPEAT_INFINITE);
      creditsMusic->Play( DMUS_SEGF_SECONDARY );  
    }
  }
}
void SoundManager::StopMusic() {
  if( musicManager ) {
    if( creditsMusic ) {
      creditsMusic->Stop();  
    }
  }
}
