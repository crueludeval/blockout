/*
 	File:        PolyCube.h
  Description: PolyCube management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Types.h"

#ifndef POLYCUBEH
#define POLYCUBEH

class PolyCube
{
  public:
    PolyCube();
    ~PolyCube();

    // Add a cube to the polycube
    void AddCube(int x,int y,int z);

    // Set polycube info
    void SetInfo(int highScore,int lowScore,BOOL flat,BOOL basic);

    // Create polycube device objects
    HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice,float cubeSide,VERTEX origin,int ghost,float wEdge = 0.0f);

    // Render the polycube
    void Render(LPDIRECT3DDEVICE8 pd3dDevice,BOOL redMode,BOOL antialias);

    // Release device objects
    void InvalidateDeviceObjects();

    // Return the rotation center
    D3DXVECTOR3 GetRCenter();
    BLOCKITEM   GetICenter();

    // Copy the polycube cube coordinates
    void CopyCube(BLOCKITEM *c,int *nb);

    // Get number of cube
    int GetNbCube();

    // Get High score
    int GetHighScore();

    // Get Low score
    int GetLowScore();

    // Check if the polycube belongs to the set
    BOOL IsInSet(int set);

    // Get dimension
    int GetWidth();
    int GetHeight();
    int GetDepth();
    int GetMaxDim();

		//Initialise rotation center
		void InitRotationCenter();

		// Orientation stuff (Used by AI player)
    void AddOrientation(int r0,int r1,int r2);
		ORIENTATION *GetOrientationAt(int idx);
		int GetNbOrientation();

  private:

    int  hScore;     // score (dropped from top position)
    int  lScore;     // score (non dropped)
    BOOL isFlat;     // Into the flat set
    BOOL isBasic;    // Into the basic set
    BOOL hasGhost;   // PolyCube transparent side
    BOOL hasBigEdge; // PolyCube cylinder edge
    float cubeSide;  // Cube side length
    VERTEX origin;   // Origin
    D3DXVECTOR3 center; // Rotation center (space coordinates)
    BLOCKITEM iCenter;  // Rotation center (cube coordinates)

    // Cube coordinates
    BLOCKITEM cubes[MAX_CUBE];
    int nbCube;

		// Edge list (avoid edge duplication)
		EDGE *edges;
		int  nbEdge;

		// Possible orientation array
		ORIENTATION *allRot;
		int nbOrientation;

		// Big edge corner
    void GetEdgesAtCorner(int x,int y,int z,int *nb,int *direction);
		BOOL IsPermut(int d1,int d2,int d3,int *dir);
    BOOL GetCornerAt(int x,int y,int z,VERTEX *o);

    // Graphics stuff
    LPDIRECT3DVERTEXBUFFER8 vbf;
    LPDIRECT3DINDEXBUFFER8  idx;
    int nbVertex;
    int nbIndex;

    LPDIRECT3DVERTEXBUFFER8 bigEdgeVbf;
    LPDIRECT3DINDEXBUFFER8  bigEdgeIdx;
    int nbBigEdgeVertex;
    int nbBigEdgeIndex;

    LPDIRECT3DVERTEXBUFFER8 ghostVbf;
    LPDIRECT3DINDEXBUFFER8  ghostIdx;
    int nbGhostVertex;
    int nbGhostIndex;
    LPDIRECT3DTEXTURE8 ghostTexture;

    D3DMATERIAL8 whiteMaterial;
    D3DMATERIAL8 ghostMaterial;
    D3DMATERIAL8 redMaterial;
    D3DMATERIAL8 grayMaterial;

    TFACEVERTEX gV(int cubeIdx,int x,int y,int z,float nx,float ny,float nz,float tu,float tv);
    HRESULT CreateGhost(LPDIRECT3DDEVICE8 pd3dDevice,int value);
    HRESULT CreateLineEdge(LPDIRECT3DDEVICE8 pd3dDevice);
    HRESULT CreateCylinderEdges(LPDIRECT3DDEVICE8 pd3dDevice,float wEdge);
    BOOL IsEdgeVisible(int cubeIdx,int edge);
    BOOL IsFaceVisible(int cubeIdx,int face);
    BOOL FindCube(int x,int y,int z);
		BOOL EdgeExist(EDGE e);
    BOOL EdgeEqual(EDGE e1,EDGE e2);

};

#endif /* POLYCUBEH */
