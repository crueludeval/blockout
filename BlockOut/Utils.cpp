/*
 	File:        Utils.cpp
  Description: Various util functions
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Types.h"
#include <time.h>
#include <stdio.h>
#include <Cimage.h>

//-----------------------------------------------------------------------------
// Name: v()
// Desc: Construct a VERTEX.
//-----------------------------------------------------------------------------
VERTEX v(float x,float y,float z) {

  static VERTEX ret;
  ret.x = x;
  ret.y = y;
  ret.z = z;
  return ret;

}

//-----------------------------------------------------------------------------
// Name: v()
// Desc: Construct a FACEVERTEX.
//-----------------------------------------------------------------------------
FACEVERTEX v(float x,float y,float z,float nx,float ny,float nz) {

  static FACEVERTEX ret;
  ret.x = x;
  ret.y = y;
  ret.z = z;
  ret.nx = nx;
  ret.ny = ny;
  ret.nz = nz;
  return ret;

}

//-----------------------------------------------------------------------------
// Name: v()
// Desc: Construct a TFACEVERTEX.
//-----------------------------------------------------------------------------
TFACEVERTEX v(float x,float y,float z,float nx,float ny,float nz,float tu,float tv) {

  static TFACEVERTEX ret;
  ret.x = x;
  ret.y = y;
  ret.z = z;
  ret.nx = nx;
  ret.ny = ny;
  ret.nz = nz;
  ret.tu = tu;
  ret.tv = tv;
  return ret;

}

//-----------------------------------------------------------------------------
// Name: fround()
// Desc: Round to the nearest integer
//-----------------------------------------------------------------------------
int fround(float x) {

  int i;
  if( x<0.0f ) {
    i = (int)( (-x) + 0.5f );
    i = -i;
  } else {
    i = (int)( x + 0.5f );
  }

  return i;

}

//-----------------------------------------------------------------------------
// Name: SetPix()
// Desc: Set a pixel on a D3D surface (color = ARGB)
//-----------------------------------------------------------------------------
void SetPix(D3DLOCKED_RECT *d3dlr,D3DFORMAT format,int x,int y,DWORD color) {

   DWORD red,green,blue;
   WORD  data16;
   DWORD data32;

   blue  = (color & 0xFF);
   green = (color & 0xFF00) >> 8;
   red   = (color & 0xFF0000) >> 16;

   switch( format ) {

     case D3DFMT_X8R8G8B8:
          data32 = blue | green << 8 | red << 16;		     
          ((DWORD*)d3dlr->pBits)[(d3dlr->Pitch/4)*y+x] = data32;
          break;

     case D3DFMT_R5G6B5:
          blue  = blue  >> 3;          
          green = green >> 2; green = green << 5;
          red   = red   >> 3; red   = red   << 11;
          data16= (WORD) ( blue | green | red );		     
          ((WORD*)d3dlr->pBits)[(d3dlr->Pitch/2)*y+x] = data16;
          break;

     case D3DFMT_X1R5G5B5:
          blue  = blue  >> 3;          
          green = green >> 3; green = green << 5;
          red   = red   >> 3; red   = red   << 10;
          data16  = (WORD) ( blue | green | red );          
          ((WORD*)d3dlr->pBits)[(d3dlr->Pitch/2)*y+x] = data16;
          break;

   }

}

//-----------------------------------------------------------------------------
// Name: FormatTime(float seconds)
// Desc: Format time as XXmin YYsec
//-----------------------------------------------------------------------------
char *FormatTime(float seconds) {

  static char ret[32];
  int min = (int)seconds / 60;
  int sec = (int)seconds % 60;
  sprintf(ret,"%dmin %02dsec",min,sec);

  return ret;

}

//-----------------------------------------------------------------------------
// Name: FormatDate(int time)
// Desc: Format date as DD-MM-YYYY HH:MM:SS
//-----------------------------------------------------------------------------
char *FormatDate(int time) {

  static char ret[32];
  if( time>0 ) {
#ifdef LOCALTIME32
    struct tm *ts = _localtime32((__time32_t *)&time);
#else
    struct tm *ts = localtime((time_t *)&time);
#endif
    sprintf(ret,"%02d-%02d-%04d %02d:%02d:%02d",ts->tm_mday,ts->tm_mon+1,ts->tm_year+1900,
                                                ts->tm_hour,ts->tm_min,ts->tm_sec);
  } else {
    strcpy(ret,"");
  }

  return ret;

}

//-----------------------------------------------------------------------------
// Name: FormatDate(int time)
// Desc: Format date as DD-MM-YYYY
//-----------------------------------------------------------------------------
char *FormatDateShort(int time) {

  static char ret[32];
  if( time>0 ) {
#ifdef LOCALTIME32
    struct tm *ts = _localtime32((__time32_t *)&time);
#else
    struct tm *ts = localtime((time_t *)&time);
#endif
    sprintf(ret,"%02d-%02d-%04d",ts->tm_mday,ts->tm_mon+1,ts->tm_year+1900);
  } else {
    strcpy(ret,"..........");
  }

  return ret;

}

//-----------------------------------------------------------------------------
// Name: CreateColorTexture()
// Desc: Create a color texture (with alpha)
//-----------------------------------------------------------------------------
HRESULT CreateColorTexture(LPDIRECT3DDEVICE8 pd3dDevice,int width,int height,D3DFORMAT format,DWORD color,BYTE alpha,LPDIRECT3DTEXTURE8 *hmap) {

  HRESULT        hr;
  D3DLOCKED_RECT d3dlr;
  D3DFORMAT      tex_format;
  *hmap = NULL;

  switch( format ) {
    case D3DFMT_X8R8G8B8:
      tex_format = D3DFMT_A8R8G8B8;
      break;
    case D3DFMT_X1R5G5B5:
    case D3DFMT_R5G6B5:
      tex_format = D3DFMT_A4R4G4B4;
      break;
    default:
      return D3DERR_WRONGTEXTUREFORMAT;
      break;
  }

  hr = D3DXCreateTexture(pd3dDevice,width,height,1,0,tex_format,D3DPOOL_MANAGED,hmap);
  if( FAILED(hr) ) return hr;

  hr = (*hmap)->LockRect( 0, &d3dlr, NULL, 0 );
  if( FAILED(hr) ) return hr;

  DWORD red,green,blue,walpha;
  blue   = (color & 0xFF);
  green  = (color & 0xFF00) >> 8;
  red    = (color & 0xFF0000) >> 16;
  walpha = (DWORD)alpha;

  DWORD data32 = walpha << 24 | red << 16 | green << 8 | blue;
  blue  = blue  >> 4;          
  green = green >> 4; green = green << 4;
  red   = red   >> 4; red   = red   << 8;
  walpha = walpha >> 4; walpha = walpha << 12;
  WORD data16  = (WORD) ( walpha | blue | green | red );          

  for(int j=0;j<height;j++)
  {
    for(int i=0;i<width;i++ )
    {
      switch( tex_format ) {
        case D3DFMT_A8R8G8B8:
          ((DWORD*)d3dlr.pBits)[(d3dlr.Pitch/4)*j+i] = data32;
          break;
        case D3DFMT_A4R4G4B4:
          ((WORD*)d3dlr.pBits)[(d3dlr.Pitch/2)*j+i] = data16;
          break;
      }
    }
  }

  (*hmap)->UnlockRect(0);

  return S_OK;
}

//-----------------------------------------------------------------------------
// Name: CreateTexture()
// Desc: Create a texture (no alpha)
//-----------------------------------------------------------------------------
HRESULT CreateTexture(LPDIRECT3DDEVICE8 pd3dDevice,int width,int height,D3DFORMAT format,char *imgName,LPDIRECT3DTEXTURE8 *hmap) {

  HRESULT        hr;
  D3DLOCKED_RECT d3dlr;
  *hmap = NULL;
  CImage img;

  switch( format ) {
    case D3DFMT_X8R8G8B8:
    case D3DFMT_X1R5G5B5:
    case D3DFMT_R5G6B5:
      break;
    default:
      return D3DERR_WRONGTEXTUREFORMAT;
  }

  if( !img.LoadImage(imgName) ) {

    char err_msg[512];
    sprintf( err_msg , "CreateTexture(): %s\nFile: %s",img.GetErrorMessage(),imgName );
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;

  }

  if( img.Width() != width || img.Height() != height ) {

    char err_msg[512];
    sprintf( err_msg , "CreateTexture(): Wrong image dimension (%dx%d required)\nFile: %s",width,height,imgName );
    MessageBox(NULL,err_msg,"Error",MB_OK);
    return E_FAIL;

  }

  hr = D3DXCreateTexture(pd3dDevice,width,height,1,0,format,D3DPOOL_MANAGED,hmap);
  if( FAILED(hr) ) return hr;

  hr = (*hmap)->LockRect( 0, &d3dlr, NULL, 0 );
  if( FAILED(hr) ) return hr;

  DWORD red,green,blue;
  DWORD add=0;
  DWORD data32;
  WORD  data16;
  BYTE *data = img.GetData();

  for(int j=0;j<height;j++)
  {
    for(int i=0;i<width;i++ )
    {

      red   =(DWORD)data[add+2];
      green =(DWORD)data[add+1];
      blue  =(DWORD)data[add+0];

      switch( format ) {

        case D3DFMT_X8R8G8B8:

          data32 = blue | green << 8 | red << 16;		     
          ((DWORD*)d3dlr.pBits)[(d3dlr.Pitch/4)*j+i] = data32;
          break;

        case D3DFMT_R5G6B5:
          blue  = blue  >> 3;          
          green = green >> 2; green = green << 5;
          red   = red   >> 3; red   = red   << 11;
          data16= (WORD) ( blue | green | red );		     
          ((WORD*)d3dlr.pBits)[(d3dlr.Pitch/2)*j+i] = data16;
          break;

        case D3DFMT_X1R5G5B5:

          blue  = blue  >> 3;          
          green = green >> 3; green = green << 5;
          red   = red   >> 3; red   = red   << 10;
          data16  = (WORD) ( blue | green | red );          
          ((WORD*)d3dlr.pBits)[(d3dlr.Pitch/2)*j+i] = data16;
          break;

      }

      add+=3;
    }
  }

  (*hmap)->UnlockRect(0);
  img.Release();

  return S_OK;
}

//-----------------------------------------------------------------------------
// Name: GetChar()
// Desc: Return char according to pressed key
//-----------------------------------------------------------------------------
extern char GetChar(BYTE *keys,BOOL editMaj) {

  char retChar = 0;

  // Check if number are obtained with shift
  short nbScan      = VkKeyScan('1');
  BOOL  numberShift = (nbScan & 0x100)>0;
  short arobasScan  = VkKeyScan('@');
  BOOL  arobasShift = (arobasScan & 0x100)>0;
  short divideScan  = VkKeyScan('/');
  BOOL  divideShift = (divideScan & 0x100)>0;
  short dotScan     = VkKeyScan(':');
  BOOL  dotShift    = (dotScan & 0x100)>0;
  short quotScan    = VkKeyScan('\'');
  BOOL  quotShift   = (quotScan & 0x100)>0;
  short excScan      = VkKeyScan('!');
  BOOL  excShift     = (excScan & 0x100)>0;
  short dolScan      = VkKeyScan('$');
  BOOL  dolShift     = (dolScan & 0x100)>0;
  short perScan      = VkKeyScan('%');
  BOOL  perShift     = (perScan & 0x100)>0;
  short ampScan      = VkKeyScan('&');
  BOOL  ampShift     = (ampScan & 0x100)>0;
  short mulScan      = VkKeyScan('*');
  BOOL  mulShift     = (mulScan & 0x100)>0;
  short opbScan      = VkKeyScan('(');
  BOOL  opbShift     = (opbScan & 0x100)>0;
  short cpbScan      = VkKeyScan(')');
  BOOL  cpbShift     = (cpbScan & 0x100)>0;
  short undScan      = VkKeyScan('_');
  BOOL  undShift     = (undScan & 0x100)>0;
  short queScan      = VkKeyScan('?');
  BOOL  queShift     = (queScan & 0x100)>0;
  short scoScan      = VkKeyScan(';');
  BOOL  scoShift     = (scoScan & 0x100)>0;

  // Check letters
  for(BYTE i='A';i<='Z';i++) {
    if( keys[i] ) {
      if( editMaj )
        retChar = toupper(i);
      else
        retChar = tolower(i);
    }
  }

  // Check numbers (NUMPAP)
  for(BYTE i=0;i<=9;i++) {
    if( keys[i+VK_NUMPAD0] ) {
      retChar = (char)i+'0';
    }
  }

  // Check numbers (Keyboard)
  if( numberShift == editMaj ) {
    for(BYTE i=0;i<=9;i++) {
      if( keys[i+'0'] ) {
        retChar = (char)i+'0';
      }
    }
  }

  // Space
  if( keys[VK_SPACE] ) {
    retChar = ' ';
  }

  // Dot
  if( keys[VK_OEM_PERIOD] || keys[VK_DECIMAL] ) {
    retChar = '.';
  }

  // Comma
  if( keys[VK_OEM_COMMA] ) {
    retChar = ',';
  }
  
  // Minus
  if( keys[VK_OEM_MINUS] || keys[VK_SUBTRACT]  ) {
    retChar = '-';
  }

  // Plus
  if( keys[VK_OEM_PLUS] || keys[VK_ADD]  ) {
    retChar = '+';
  }

  // Divide (NUMPAD)
  if( keys[VK_DIVIDE] ) {
    retChar = '/';
  }

  // ':'
  if( (dotShift==editMaj) && keys[dotScan&0xFF] ) {
    retChar = ':';
  }

  // '@'
  if( (arobasShift==editMaj) && keys[arobasScan&0xFF] ) {
    retChar = '@';
  }

  // '/'
  if( (divideShift==editMaj) && keys[divideScan&0xFF] ) {
    retChar = '/';
  }

  // '''
  if( (quotShift==editMaj) && keys[quotScan&0xFF] ) {
    retChar = '\'';
  }

  // '!'
  if( (excShift==editMaj) && keys[excScan&0xFF] ) {
    retChar = '!';
  }

  // '$'
  if( (dolShift==editMaj) && keys[dolScan&0xFF] ) {
    retChar = '$';
  }

  // '%'
  if( (perShift==editMaj) && keys[perScan&0xFF] ) {
    retChar = '%';
  }

  // '&'
  if( (ampShift==editMaj) && keys[ampScan&0xFF] ) {
    retChar = '&';
  }

  // '*'
  if( (mulShift==editMaj) && keys[mulScan&0xFF] ) {
    retChar = '*';
  }

  // '('
  if( (opbShift==editMaj) && keys[opbScan&0xFF] ) {
    retChar = '(';
  }

  // ')'
  if( (cpbShift==editMaj) && keys[cpbScan&0xFF] ) {
    retChar = ')';
  }

  // '_'
  if( (undShift==editMaj) && keys[undScan&0xFF] ) {
    retChar = '_';
  }

  // '?'
  if( (queShift==editMaj) && keys[queScan&0xFF] ) {
    retChar = '?';
  }

  // ';'
  if( (scoShift==editMaj) && keys[scoScan&0xFF] ) {
    retChar = ';';
  }

  // Reset keys
  for(BYTE i='A';i<='Z';i++)
    keys[i]=0;

  for(BYTE i=0;i<=9;i++) {
    keys[i+VK_NUMPAD0]=0;
    keys[i+'0']=0;
  }

  keys[VK_SPACE]=0;
  keys[VK_OEM_PERIOD]=0;
  keys[VK_DECIMAL]=0;
  keys[VK_OEM_COMMA]=0;
  keys[VK_OEM_MINUS]=0;
  keys[VK_SUBTRACT]=0;
  keys[VK_OEM_PLUS]=0;
  keys[VK_ADD]=0;
  keys[VK_DIVIDE]=0;
  keys[dotScan&0xFF]=0;
  keys[arobasScan&0xFF]=0;
  keys[divideScan&0xFF]=0;
  keys[quotScan&0xFF]=0;
  keys[excScan&0xFF]=0;
  keys[dolScan&0xFF]=0;
  keys[perScan&0xFF]=0;
  keys[ampScan&0xFF]=0;
  keys[mulScan&0xFF]=0;
  keys[opbScan&0xFF]=0;
  keys[cpbScan&0xFF]=0;
  keys[undScan&0xFF]=0;
  keys[queScan&0xFF]=0;
  keys[scoScan&0xFF]=0;

  return retChar;

}
