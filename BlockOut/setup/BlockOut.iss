; -- BlockOut Installation script for Inno Setup --
;

[Setup]
AppName=BlockOut II
AppVerName=BlockOut 2.3
AppCopyright=JL Pons
DefaultDirName={pf}\blockout
DefaultGroupName=BlockOut
WizardImageFile=setup.bmp
WizardImageBackColor=$393A21

[Files]
Source: "../README.txt"; DestDir: "{app}"; CopyMode: alwaysoverwrite; Flags: isreadme;
Source: "../VC7/Release/BlockOut.exe"; DestDir: "{app}"; CopyMode: alwaysoverwrite;
Source: "../images/background.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/background2.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/background3.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/menuback.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/menufont.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/menufont2.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/menupit.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/sprites.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/spritesa.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/spark.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/sparka.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/gameover.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/gameovera.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/menucredits.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/star_crystal_grid.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/online.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/onlinea.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/marble.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../images/marbleg.png"; DestDir: "{app}/images"; CopyMode: alwaysoverwrite;
Source: "../sounds/blub.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/empty.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/level.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/line.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/tchh.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/welldone.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/empty2.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/level2.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/line2.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/welldone2.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/wozz.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/hit.wav"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "../sounds/music.mp3"; DestDir: "{app}/sounds"; CopyMode: alwaysoverwrite;
Source: "decmp3.exe"; DestDir: "{app}"; CopyMode: alwaysoverwrite; Flags: deleteafterinstall;


[Icons]
Name:"{group}\BlockOut II"; Filename:"{app}/BlockOut.exe"; WorkingDir: "{app}";
Name:"{group}\Readme"; Filename:"{app}/README.txt"; WorkingDir: "{app}";
Name:"{userdesktop}\BlockOut II" ; Filename:"{app}/BlockOut.exe"; WorkingDir: "{app}";

[Run]
Filename: "{app}\decmp3.exe"; WorkingDir: "{app}";Flags: runhidden;

[UninstallDelete]
Type: files; Name: "{app}/sounds/music.wav";
Type: dirifempty; Name: "{app}/sounds";

