/*
 	File:        Game.cpp
  Description: Game management
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#include "Game.h"
#include <time.h>

// Score calculation factors, used by Game::ComputeScore()
// (coming from measurements made with the original BlockOut game)

// PolyCube Level Factor (from level 0 to 10)
const float pLevelFactor[] = { 0.066990f , 0.139195f , 0.219800f , 0.308444f , 0.403897f , 
                               0.507822f , 0.619062f , 0.738630f , 0.865802f , 1.000000f ,
                               1.133333f };

// Depth Factor (from depth 6 to 18)
const float depthFactor[] = { 0.0f , 0.0f , 0.0f , 0.0f , 0.0f , 0.0f , 
                              1.557692f, 1.367521f, 1.217949f, 1.100427f, 1.000000f,
                              0.918803f, 0.852564f, 0.788996f, 0.737714f, 0.691774f, 
                              0.651709f, 0.614850f, 0.583868f };

// Line Level Factor (from level 0 to 10)
const float lLeveFactor[] = { 0.096478f, 0.163873f, 0.242913f, 0.328261f, 0.422329f,
                              0.518394f, 0.630405f, 0.747501f, 0.867087f, 1.000000f,
                              1.131653f };

// Line number factor (from 1 to 5)
const float lNumberFactor[] = { 0.0f , 1.000000f, 3.703372f, 8.104827f, 14.188325f, 22.144941f };

// Line base score (FLAT,BASIC,EXTENDED)
const float lineBase[] = { 762.5f , 875.5f , 2886.25f };

// Step Time base (seconds)
const float timeBase = 5.51f;

// Time Level factor
const float timeLevelFactor = 0.64f;

// Drop time (seconds)
const float dropTime = 0.16f;

// Spark time
const float sparkTime = 0.5f;

// ---------------------------------------------------------------------

Game::Game() {

    backgroundSurf = NULL;
    transparent = 0;
    style = 0;
    inited = FALSE;
    pFont = NULL;
    D3DXMatrixIdentity(&matView);

    // Init default rotation matrix

    matRotOx = D3DXMATRIX( 1.0f , 0.0f , 0.0f , 0.0f ,
                           0.0f , 0.0f , 1.0f , 0.0f ,
                           0.0f ,-1.0f , 0.0f , 0.0f ,
                           0.0f , 0.0f , 0.0f , 1.0f );

    matRotOy = D3DXMATRIX( 0.0f , 0.0f , 1.0f , 0.0f ,
                           0.0f , 1.0f , 0.0f , 0.0f ,
                          -1.0f , 0.0f , 0.0f , 0.0f ,
                           0.0f , 0.0f , 0.0f , 1.0f );

    matRotOz = D3DXMATRIX( 0.0f , 1.0f , 0.0f , 0.0f ,
                          -1.0f , 0.0f , 0.0f , 0.0f ,
                           0.0f , 0.0f , 1.0f , 0.0f ,
                           0.0f , 0.0f , 0.0f , 1.0f );
                           
    matRotNOx = D3DXMATRIX( 1.0f , 0.0f , 0.0f , 0.0f ,
                            0.0f , 0.0f ,-1.0f , 0.0f ,
                            0.0f , 1.0f , 0.0f , 0.0f ,
                            0.0f , 0.0f , 0.0f , 1.0f );

    matRotNOy = D3DXMATRIX( 0.0f , 0.0f ,-1.0f , 0.0f ,
                            0.0f , 1.0f , 0.0f , 0.0f ,
                            1.0f , 0.0f , 0.0f , 0.0f ,
                            0.0f , 0.0f , 0.0f , 1.0f );

    matRotNOz = D3DXMATRIX( 0.0f ,-1.0f , 0.0f , 0.0f ,
                            1.0f , 0.0f , 0.0f , 0.0f ,
                            0.0f , 0.0f , 1.0f , 0.0f ,
                            0.0f , 0.0f , 0.0f , 1.0f );

}

// ---------------------------------------------------------------------

void Game::SetSetupManager(SetupManager *m) {

  setupManager = m;

}

// ---------------------------------------------------------------------

void Game::SetSoundManager(SoundManager *manager) {

  soundManager = manager;

}

// ---------------------------------------------------------------------

HRESULT Game::Create(LPDIRECT3DDEVICE8 pd3dDevice) {

    // --------------------------------------------------------------

    // Check pit dimension
    int ok = thePit.GetWidth() * thePit.GetHeight() * thePit.GetDepth();
    if(ok==0) return S_OK;

    // --------------------------------------------------------------
    char err_msg[1024];
    HRESULT hr;

    hr = thePit.Create(pd3dDevice,style);

	  if(FAILED(hr)) {
	    sprintf(err_msg,"Game::CreatePit failed:%s",GetDDErrorMsg(hr));
	    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
      return E_FAIL;
    }

    // --------------------------------------------------------------

    // Get back buffer dimemsion
    D3DSURFACE_DESC desc;
    LPDIRECT3DSURFACE8 pBackBuffer;
    pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );
    pBackBuffer->GetDesc( &desc );
    pBackBuffer->Release();

    // Compute pit viewport
    pitView.X      = fround( (float)desc.Width  * 0.0889f );
    pitView.Y      = fround( (float)desc.Height * 0.0183f );
    pitView.Width  = fround( (float)desc.Width  * 0.7197f );
    pitView.Height = fround( (float)desc.Height * 0.9596f );
    pitView.MinZ   = 0.0f;
    pitView.MaxZ   = 1.0f;

    // Sprite viewport
    spriteView.X = 0;
    spriteView.Y = 0;
    spriteView.Width = desc.Width;
    spriteView.Height = desc.Height;
    spriteView.MinZ = 0.0f;
    spriteView.MaxZ = 1.0f;

    // Projection matrix
    FLOAT fAspectRatio = 1.0f;
    D3DXMatrixPerspectiveFovRH( &matProj, D3DXToRadian(60.0f), fAspectRatio, 0.1f, FAR_DISTANCE );

    // --------------------------------------------------------------
    
    if( desc.Width==1024 && desc.Height==768 && style!=STYLE_ARCADE ) {

      // CopyRect technique for background (No resize)

      // Load image
      CImage img;
      switch(style) {
        case STYLE_CLASSIC:
          if( !img.LoadImage("images\\background.png") ) {
            sprintf(err_msg,"Game::LoadImage failed\n%s",img.GetErrorMessage());
	          MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
            return E_FAIL;
          }
          break;
        case STYLE_MARBLE:
          if( !img.LoadImage("images\\background2.png") ) {
            sprintf(err_msg,"Game::LoadImage failed\n%s",img.GetErrorMessage());
	          MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
            return E_FAIL;
          }
          break;
      }

      // Create surface      
      hr = pd3dDevice->CreateImageSurface( 1024 , 768 , desc.Format , &backgroundSurf );
      if(FAILED(hr)) {
	      sprintf(err_msg,"Game::CreateImageSurface failed:%s",GetDDErrorMsg(hr));
	      MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
        return E_FAIL;
      }

      // Lock
      D3DLOCKED_RECT  d3dlr;
      HRESULT hr = backgroundSurf->LockRect( &d3dlr, NULL, 0 );
      if( FAILED( hr ) ) {
	      sprintf(err_msg,"Game::CreateImageSurface failed:%s",GetDDErrorMsg(hr));
	      MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
        return E_FAIL;
      }

      // Blit image
      DWORD  add = 0;
      BYTE  *buffer;
      DWORD  data32;

      buffer = img.GetData();
      for(int j=0;j<768;j++) {
        for(int i=0;i<1024;i++) {
          data32  = buffer[add+2]; data32 = data32 << 8;
          data32 += buffer[add+1]; data32 = data32 << 8;
          data32 += buffer[add+0];
          SetPix(&d3dlr,desc.Format,i,j,data32);
          add += 3;
        }
      }

      img.Release();
      backgroundSurf->UnlockRect();

    } else {

      // 2DSprite technique for background (resize)
      switch(style) {
        case STYLE_CLASSIC:
          hr = background.Create2DSprite(pd3dDevice,"images\\background.png","none",0,0,desc.Width,desc.Height);
          break;
        case STYLE_MARBLE:
          hr = background.Create2DSprite(pd3dDevice,"images\\background2.png","none",0,0,desc.Width,desc.Height);
          break;
        case STYLE_ARCADE:
          hr = background.Create2DSprite(pd3dDevice,"images\\background3.png","none",0,0,desc.Width,desc.Height);
          break;
      }
      background.SetSpriteMapping(0.0f,0.0f,1.0f,0.75f);

	    if(FAILED(hr)) {
	      sprintf(err_msg,"Game::CreateBackground failed:%s",GetDDErrorMsg(hr));
	      MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
        return E_FAIL;
      }

    }

    // --------------------------------------------------------------

    hr = sprites.Create(pd3dDevice,desc.Width,desc.Height);

	  if(FAILED(hr)) {
	    sprintf(err_msg,"Game::CreateSprites failed:%s",GetDDErrorMsg(hr));
	    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
      return E_FAIL;
    }

    // --------------------------------------------------------------

    hr = spark.Create2DSprite(pd3dDevice,"images\\spark.png","images\\sparka.png",0,0,0,0);

   if(FAILED(hr)) {
     sprintf(err_msg,"Game::CreateSpark failed:%s",GetDDErrorMsg(hr));
     MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
      return E_FAIL;
    }

    // --------------------------------------------------------------

  	hr = InitPolyCube(pd3dDevice,transparent,setupManager->GetLineRadius());

    if(FAILED(hr)) {
	    sprintf(err_msg,"Game::InitPolyCube failed:%s",GetDDErrorMsg(hr));
	    MessageBox(NULL,err_msg,"[BlockOut] Error",MB_OK);
      return E_FAIL;
    }

    // --------------------------------------------------------------

    int fSize = fround((float)desc.Width/60.23f);
    //pFont = new CD3DFont( _T("Courier New"), fSize, D3DFONT_BOLD );
    pFont = new CD3DFont( _T("Courier New"), 11, 0 );
    pFont->InitDeviceObjects(pd3dDevice);
    pFont->RestoreDeviceObjects();

    // --------------------------------------------------------------

    FullRepaint();

    inited = TRUE;

    return S_OK;

}

// ---------------------------------------------------------------------

void Game::SetViewMatrix(D3DXMATRIX *mView) {

  memcpy(&matView , mView , sizeof(D3DXMATRIX));

}

// ---------------------------------------------------------------------

void Game::Render(LPDIRECT3DDEVICE8 pd3dDevice) {

  if( !inited ) return;

  LPDIRECT3DSURFACE8 pBackBuffer = NULL;

  D3DXMATRIX matIdentity;
  D3DXMatrixIdentity(&matIdentity);

  pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );

  if( fullRepaint>0 || style==STYLE_ARCADE || gameMode==GAME_OVER ) {

    pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_FALSE);

    pd3dDevice->GetBackBuffer( 0, D3DBACKBUFFER_TYPE_MONO, &pBackBuffer );

    // Sprite viewport
    pd3dDevice->SetViewport(&spriteView);

    // Background
    if( backgroundSurf ) {
      pd3dDevice->CopyRects(backgroundSurf,NULL,0,pBackBuffer,NULL);
    } else {
      background.Render(pd3dDevice);
    }

    // Score
    sprites.RenderInfo(pd3dDevice,highScore,thePit.GetWidth(),thePit.GetHeight(),
                       thePit.GetDepth(),setupManager->GetBlockSet());
    thePit.RenderLevel(pd3dDevice,pBackBuffer);
    sprites.RenderScore(pd3dDevice,score.score,level,score.nbCube);
    if( demoFlag )
      sprites.RenderDemo(pd3dDevice);
    if( practiceFlag )
      sprites.RenderPractice(pd3dDevice);

    fullRepaint--;

    if( pBackBuffer )
      pBackBuffer->Release();

  }

  pd3dDevice->SetViewport(&pitView);

  // Draw pit
  if( endAnimStarted ) {
    pd3dDevice->Clear( 0L, NULL, D3DCLEAR_ZBUFFER,0x0, 1.0f, 0L );
    pd3dDevice->SetTransform( D3DTS_VIEW, &pitMatrix );
    pd3dDevice->SetTransform( D3DTS_WORLD, &matIdentity );
    thePit.Render(pd3dDevice,TRUE,TRUE);
    pd3dDevice->SetTransform( D3DTS_VIEW, &matView );
  } else {
    pd3dDevice->SetTransform( D3DTS_WORLD, &matIdentity );
    thePit.Render(pd3dDevice,gameMode!=GAME_PAUSED,FALSE);
  }

  // Draw "PAUSE" or "GAME OVER"
  if( gameMode==GAME_OVER || gameMode==GAME_PAUSED ) {
    pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_FALSE);
    pd3dDevice->SetViewport(&spriteView);
    sprites.RenderGameMode(pd3dDevice,gameMode);
    pd3dDevice->SetViewport(&pitView);
  }

	if( practiceFlag ) {

#ifdef _DEBUG
    // AI debug stuff
    BLOCKITEM cubes[MAX_CUBE];
    int nbCube;
    allPolyCube[pIdx].CopyCube(cubes,&nbCube);
    TransformCube(&newMatRot,cubes,nbCube,xPos,yPos,zPos);
    pd3dDevice->SetViewport(&spriteView);
    pFont->DrawText( 150,  40, D3DCOLOR_ARGB(255,255,255,0), 
                     botPlayer.GetInfo(&thePit,cubes,nbCube) , 0);
#endif

    pd3dDevice->SetViewport(&pitView);
	  if( startShowAI>0.0f ) {
		  if( (curTime-startShowAI)>0.5f ) {
			  startShowAI = 0.0f;
		  } else {
        pd3dDevice->SetTransform( D3DTS_WORLD, &matAI );
        allPolyCube[pIdx].Render(pd3dDevice,TRUE,FALSE);
		  }
	  }

	}

  // Draw polycube
  if( gameMode!=GAME_PAUSED && gameMode!=GAME_OVER ) {
		if(lineWidth>0) pd3dDevice->Clear( 0L, NULL, D3DCLEAR_ZBUFFER,0x0, 1.0f, 0L );
    pd3dDevice->SetTransform( D3DTS_WORLD, &mat );
    allPolyCube[pIdx].Render(pd3dDevice,redMode,useAA);
  }


  // Draw spark
  if( gameMode!=GAME_PAUSED && gameMode!=GAME_OVER && startSpark!=0.0f ) {
    pd3dDevice->SetViewport(&spriteView);
    pd3dDevice->SetRenderState(D3DRS_ZENABLE,D3DZB_FALSE);
    spark.Render(pd3dDevice);
  }

}

// ---------------------------------------------------------------------

void Game::InvalidateDeviceObjects() {

    if( pFont ) {
      pFont->InvalidateDeviceObjects();
      pFont->DeleteDeviceObjects();
    }
    SAFE_DELETE(pFont);
    SAFE_RELEASE(backgroundSurf);
    background.InvalidateDeviceObjects();
    spark.InvalidateDeviceObjects();
    thePit.InvalidateDeviceObjects();
    sprites.InvalidateDeviceObjects();
    for(int i=0;i<NB_POLYCUBE;i++)
      allPolyCube[i].InvalidateDeviceObjects();
    inited = FALSE;

}

// ---------------------------------------------------------------------

void Game::StartGame(LPDIRECT3DDEVICE8 pd3dDevice,float fTime) {

    // Recreate device objects if pit dimensions or style have changed
    if( setupManager->GetPitWidth()!=thePit.GetWidth()   ||
        setupManager->GetPitHeight()!=thePit.GetHeight() ||
        setupManager->GetPitDepth()!=thePit.GetDepth() ||
        setupManager->GetTransparentFace()!=transparent ||
        setupManager->GetStyle()!=style ||
				setupManager->GetLineWidth()!=lineWidth) {

      thePit.SetDimension(setupManager->GetPitWidth(),
                          setupManager->GetPitHeight(),
                          setupManager->GetPitDepth());

      transparent = setupManager->GetTransparentFace();
      style = setupManager->GetStyle();
			lineWidth = setupManager->GetLineWidth();

      InvalidateDeviceObjects();
      Create(pd3dDevice);

    }

    // Initialise game variable
    animationTime = setupManager->GetAnimationTime();
    level = setupManager->GetStartingLevel();
    pIdx = 0;
    gameMode = GAME_PLAYING;
    thePit.Clear();
    stepTime = timeBase * powf(timeLevelFactor,(float)level);
    exitValue = 0;
    cubePerLevel = thePit.GetHeight() * 15 + thePit.GetWidth() * 15;
    highScore = setupManager->GetHighScore();
    startGameTime = fTime;
    curTime = fTime;
    memcpy(&pitMatrix , &matView , sizeof(D3DXMATRIX));
    endAnimStarted = FALSE;
    demoFlag = FALSE;
    practiceFlag = FALSE;
		useAA = setupManager->GetAntialiasLevel()!=AA_NONE;

    // Initialise score structure
    memset(&score,0,sizeof(SCOREREC));
    score.date = (int)time(NULL);
    score.startLevel = level;
    score.setupId = setupManager->GetId();

    // Get all possible polycube
    nbPossible = 0;
    int minDim = thePit.GetWidth();
    if( thePit.GetHeight() < minDim ) minDim = thePit.GetHeight();
    for(int i=0;i<NB_POLYCUBE;i++) {
      if( allPolyCube[i].IsInSet(setupManager->GetBlockSet()) ) {
        if( allPolyCube[i].GetMaxDim() <= minDim ) {
          possible[nbPossible] = i;
          nbPossible++;
        }
      }
    }

    // Set a new seed for the random number generator
    srand(GetTickCount());
		//srand(42639731);

    // Start
    FullRepaint();
    NewPolyCube(NULL);

}

// ---------------------------------------------------------------------

void Game::StartDemo(LPDIRECT3DDEVICE8 pd3dDevice,float fTime) {

  StartGame(pd3dDevice,fTime);
  botPlayer.Init(thePit.GetWidth(),thePit.GetHeight(),thePit.GetDepth(),setupManager->GetBlockSet());
  botPlayer.GetMoves(&thePit,&(allPolyCube[pIdx]),xPos,yPos,zPos,AIMoves,&nbAIMove);
  gameMode = GAME_DEMO;
  demoFlag = TRUE;

}

// ---------------------------------------------------------------------

void Game::StartPractice(LPDIRECT3DDEVICE8 pd3dDevice,float fTime) {

  StartGame(pd3dDevice,fTime);
  practiceFlag = TRUE;
	// Initialiase the bot player for the Help mode
  startShowAI = 0.0f;
  botPlayer.Init(thePit.GetWidth(),thePit.GetHeight(),thePit.GetDepth(),setupManager->GetBlockSet());

}


// ---------------------------------------------------------------------

SCOREREC *Game::GetScore() {

  return &score;

}

// ---------------------------------------------------------------------

int Game::Process(BYTE *keys,float fTime) {

  if( !inited ) return 0;

  float cSide = thePit.GetCubeSide();
  curTime = fTime;

  if( gameMode == GAME_OVER ) {
    
    // Porcess pit animation
    float pTime = fTime - startEndTime;
    if( pTime>0.0f ) {

      endAnimStarted = TRUE;

      float zc = 5.0f * cSide;
      float d  = 10.0f * cSide;
      float x;
      float y;
      float z;
      D3DXVECTOR3 vUp;

      if( pTime<4.0f ) {

        x = 0.0f;
        y = -d * (pTime/4.0f);
        z = 0.0f;//(STARTZ) * (pTime/4.0f);

        vUp.x = 0.0f;
        vUp.y = 1.0f;
        vUp.z = -1.0f * (pTime/4.0f);
        D3DXVec3Normalize(&vUp,&vUp);

      } else {

        float a = (pTime-4.0f)/2.0f;
                
        x = d  * sinf(a);
        y = -d * cosf(a);
        z = 0.0f;

        vUp.x = -x;
        vUp.y = -y;
        vUp.z = -1.0f;
        D3DXVec3Normalize(&vUp,&vUp);

      }

      D3DXVECTOR3 vEye = D3DXVECTOR3(     x,    y,     z );  // Camera pos
      D3DXVECTOR3 vAt  = D3DXVECTOR3(  0.0f, 0.0f,  zc + STARTZ );  // Camara target
      D3DXMatrixLookAtRH( &pitMatrix, &vEye, &vAt, &vUp  );

    }

  }

  switch( gameMode ) {

    case GAME_PLAYING:
    case GAME_PAUSED:
    case GAME_OVER:
      HandleKey(keys);
      break;

    case GAME_DEMO: {

      // Abort the demo
      if( keys[VK_ESCAPE] ) {
        score.gameTime = curTime - startGameTime;
        gameMode = GAME_OVER;
        keys[VK_ESCAPE]=0;
        return 2;
      }

      // Wait end of move
      float aTime = curTime - lastAIMoveTime;
      if( aTime>animationTime && rotateMode==0 ) {

        // Execute move

        if( curAIMove>=nbAIMove ) {

          // Drop
          StartDrop();

        } else {

          startRotateTime = curTime;
          rotateMode = AIMoves[curAIMove].rotate;
          switch( rotateMode ) {
           case 1:
             D3DXMatrixMultiply(&newMatRot,&matRot,&matRotOx);
             break;
           case 2:
             D3DXMatrixMultiply(&newMatRot,&matRot,&matRotOy);
             break;
           case 3:
             D3DXMatrixMultiply(&newMatRot,&matRot,&matRotOz);
             break;
           case 4:
             D3DXMatrixMultiply(&newMatRot,&matRot,&matRotNOx);
             break;
           case 5:
             D3DXMatrixMultiply(&newMatRot,&matRot,&matRotNOy);
             break;
           case 6:
             D3DXMatrixMultiply(&newMatRot,&matRot,&matRotNOz);
             break;
          }

          if( AIMoves[curAIMove].tx || AIMoves[curAIMove].ty || AIMoves[curAIMove].tz ) {
            xPos += AIMoves[curAIMove].tx;
            yPos += AIMoves[curAIMove].ty;
            zPos += AIMoves[curAIMove].tz;
            InitTranslate();
          }

          curAIMove++;

        }

        lastAIMoveTime = curTime;

      }
      } break;

  }

  if( !(gameMode==GAME_PLAYING || gameMode==GAME_DEMO) )
    return exitValue;

  // Compute rotation
  D3DXMATRIX tmpRotation;
  D3DXMatrixIdentity(&tmpRotation);

  if( rotateMode ) {
    float rTime = (fTime - startRotateTime);
    if( rTime > animationTime ) {
      // End of rotation
      matRot = newMatRot;
      rotateMode = 0;
    } else {

      float angle;
      if( rotateMode>0 ) {
        angle = PI/2.0f * (rTime/animationTime);
      } else {
        angle = PI/5.0f * sinf( (rTime/animationTime) * PI );
      }

      // Transition
      switch(rotateMode) {
        case 1:
        case -1:
          D3DXMatrixRotationX(&tmpRotation,angle);
          break;
        case 2:
        case -2:
          D3DXMatrixRotationY(&tmpRotation,-angle);
          break;
        case 3:
        case -3:
          D3DXMatrixRotationZ(&tmpRotation,angle);
          break;
        case 4:
        case -4:
          D3DXMatrixRotationX(&tmpRotation,-angle);
          break;
        case 5:
        case -5:
          D3DXMatrixRotationY(&tmpRotation,angle);
          break;
        case 6:
        case -6:
          D3DXMatrixRotationZ(&tmpRotation,-angle);
          break;
      }
    }
  }

  // Compute translation
  float tTime = (fTime - startTranslateTime);

  if( tTime > animationTime ) {
    // End of translation
    vPos.x = (float)xPos;
    vPos.y = (float)yPos;
    vPos.z = (float)zPos;
  } else {
    // Transition
    vPos = vOrgPos + (vTransPos * (tTime/animationTime));
  }

  // Compute drop
  if( dropMode ) {
    float dTime = (fTime - startTranslateTime);
    startStepTime = fTime; // No step while drop
    if( dTime > dropTime ) {
      // End of drop
      vPos.z = (float)zPos;
      dropMode = FALSE;
      // Process last key
      if( !lastKeyMode ) {
        redMode = TRUE;
        startRedTime = fTime;
      } else {
        // Fast step on drop (100 ms)
        startStepTime = fTime - stepTime + 0.1f;
        lastKeyMode = FALSE;
      }
    } else {
      vPos = vOrgPos + (vTransPos * (dTime/dropTime));
    }
  }

  // Step
  if( !practiceFlag && !redMode && (gameMode==GAME_PLAYING) ) {
    if( startStepTime==0.0f ) {
      // First step
      startStepTime = fTime;
    } else {
      float sTime = (fTime - startStepTime);
      if( sTime > stepTime ) {
        startStepTime = fTime;
        cursorPos++;
        dropPos--;
        if( IsLower() ) {
          if( !IsOverlap(&newMatRot,xPos,yPos,zPos+1) ) {
            zPos++;
            InitTranslate();
          } else {
            redMode = TRUE;
            startRedTime = fTime;
          }
        }
      }
    }
  }

  // Spark
  if( startSpark != 0.0f ) {

    float sTime = (fTime - startSpark); 
    if( sTime >= sparkTime ) {
      startSpark = 0.0f;
    } else {

      int frame = (int)( (sTime*16.0f)/sparkTime );
      if( frame>15 ) frame = 15;
      float x1 = (float)(frame % 4)/4.0f;
      float y1 = (float)(frame / 4)/4.0f;
      float x2 = x1 + 0.25f;
      float y2 = y1 + 0.25f;
      spark.SetSpriteMapping(x1,y1,x2,y2);

    }

  }
  
  // Red mode (end of move)
  if( redMode ) {
    float rTime = (fTime - startRedTime);
    if( rTime > 0.05f ) {
      AddPolyCube();
      NewPolyCube(keys);
    }
  }


  if( gameMode==GAME_OVER ) {
    // Start pit animation
    startEndTime = fTime + 0.5f;
  }

  // Compute transformation matrix
  D3DXMATRIX matT1;
  D3DXMATRIX matT2;
  D3DXMATRIX matT3;

  D3DXMatrixTranslation(&matT3,vPos.x*cSide,vPos.y*cSide,vPos.z*cSide);

  D3DXVECTOR3 center = allPolyCube[pIdx].GetRCenter();

  D3DXMatrixTranslation(&matT1,-center.x,-center.y,-center.z);
  D3DXMatrixTranslation(&matT2,center.x,center.y,center.z);

  D3DXMatrixMultiply(&mat,&matT1,&matRot);
  D3DXMatrixMultiply(&mat,&mat,&tmpRotation);
  D3DXMatrixMultiply(&mat,&mat,&matT2);
  D3DXMatrixMultiply(&mat,&mat,&matT3);

  return exitValue;
}

// ---------------------------------------------------------------------

void Game::AddPolyCube() {

   // Add the polycube to the pit
   BLOCKITEM cubes[MAX_CUBE];
   int nbCube;
   allPolyCube[pIdx].CopyCube(cubes,&nbCube);
   TransformCube(&newMatRot,cubes,nbCube,xPos,yPos,zPos);

   for(int i=0;i<nbCube;i++)
     thePit.AddCube( cubes[i].x , cubes[i].y , cubes[i].z );

   // Remove lines
   int nbLines = thePit.RemoveLines();
   BOOL pitEmpty = thePit.IsEmpty();
   if( nbLines > 0 ) {
     switch( setupManager->GetSoundType() ) {
       case SOUND_BLOCKOUT2:
         if( pitEmpty ) soundManager->PlayEmpty();
         else           soundManager->PlayLine();
         break;
       case SOUND_BLOCKOUT:
         if( pitEmpty ) soundManager->PlayEmpty2();
         else           soundManager->PlayLine2();
         break;
     }
   }

   // Upate score and level
   ComputeScore(nbLines,pitEmpty);
   if( level<10 ) {
     if( score.nbCube >= cubePerLevel * (level+1) ) {
       level++;
       stepTime *= timeLevelFactor;
       switch( setupManager->GetSoundType() ) {
         case SOUND_BLOCKOUT2:
           soundManager->PlayLevel();
           break;
         case SOUND_BLOCKOUT:
           soundManager->PlayLevel2();
           break;
       }
     }
   }

   FullRepaint();

}

// ---------------------------------------------------------------------

void Game::ComputeScore(int nbLines,BOOL pitEmpty) {

  if( !dropped )  dropPos = 0;
  if( dropPos<0 ) dropPos = 0;

  // PolyCube score
  float fPos   = (float)dropPos / (float)(thePit.GetDepth()-1);
  float lScore = (float)allPolyCube[pIdx].GetLowScore();
  float hScore = (float)allPolyCube[pIdx].GetHighScore();
  float pScore = (lScore + (hScore-lScore) * fPos) * pLevelFactor[level];

  // Line score
  float lineScore = lineBase[ setupManager->GetBlockSet() ] * lLeveFactor[level] * lNumberFactor[nbLines];

  // Empty pit ( 2 lines bonus )
  float pitScore = 0.0f;
  if( pitEmpty )
    pitScore = lineBase[ setupManager->GetBlockSet() ] * lLeveFactor[level] * lNumberFactor[2];

  // Total score
  float totalScore = ( lineScore + pScore + pitScore ) * depthFactor[ thePit.GetDepth() ];

  // Round
  int iScore = fround( totalScore );
  if( iScore<1 ) iScore = 1;

  // Update score
  score.score  += iScore;

  // Update statistics
  score.nbCube += allPolyCube[pIdx].GetNbCube();
  if(pitEmpty) score.emptyPit++;

  // Update nbLines
  switch( nbLines ) {
    case 1:
      score.nbLine1++;
      break;
    case 2:
      score.nbLine2++;
      break;
    case 3:
      score.nbLine3++;
      break;
    case 4:
      score.nbLine4++;
      break;
    case 5:
      score.nbLine5++;
      break;
  }

}

// ---------------------------------------------------------------------

void Game::NewPolyCube(BYTE *keys) {

   // Init position and rotation
   vPos = D3DXVECTOR3( 0.0f , 0.0f , 0.0f );
   xPos = 0;
   yPos = 0;
   zPos = 0;
   D3DXMatrixIdentity(&matRot);
   D3DXMatrixIdentity(&newMatRot);
   D3DXMatrixIdentity(&mat);

   // Reset keys and mode
   if( keys )
     ZeroMemory( keys, 256 * sizeof(BYTE) );
   startRotateTime = 0.0f;
   startTranslateTime = 0.0f;
   rotateMode = 0;
   dropMode = FALSE;
   lastKeyMode = FALSE;
   redMode = FALSE;
   startStepTime = 0.0f;
   dropPos = thePit.GetDepth() - 1;
   cursorPos = 0;
   dropped = FALSE;
   lastAIMoveTime = curTime;
   curAIMove = 0;

   // Select new polycube
   pIdx = SelectPolyCube();
   if( gameMode==GAME_DEMO ) 
     botPlayer.GetMoves(&thePit,&(allPolyCube[pIdx]),xPos,yPos,zPos,AIMoves,&nbAIMove);

}

// ---------------------------------------------------------------------

void Game::StartDrop() {

  if( gameMode==GAME_PAUSED || gameMode==GAME_OVER ) return;

  if( !dropMode && !redMode && !lastKeyMode ) {

     dropMode = TRUE;
     dropped = TRUE;

     // Compute translation
     int z = zPos;
     while( !IsOverlap(&newMatRot,xPos,yPos,z+1) ) z++;
     zPos = z;
     // Position the cursor to the bottom of the block
     cursorPos = GetBottom();
     InitTranslate();

  }

}

// ---------------------------------------------------------------------

void Game::InitTranslate() {

     startTranslateTime = curTime;
     vOrgPos = vPos;
     vTransPos = D3DXVECTOR3( (float)xPos - vPos.x , 
                              (float)yPos - vPos.y ,
                              (float)zPos - vPos.z );

}

// ---------------------------------------------------------------------

BOOL Game::StartTranslate(int tType) {

  if( gameMode==GAME_PAUSED || gameMode==GAME_OVER ) return TRUE;

  if( !dropMode && !redMode ) {

     switch(tType) {
        case 1: // Left
          if( !IsOverlap(&newMatRot,xPos+1,yPos,zPos) ) {
            xPos++;
            InitTranslate();
          }
          break;
        case 2: // Rigth
          if( !IsOverlap(&newMatRot,xPos-1,yPos,zPos) ) {
            xPos--;
            InitTranslate();
          }
          break;
        case 3: // Up
          if( !IsOverlap(&newMatRot,xPos,yPos+1,zPos) ) {
            yPos++;
            InitTranslate();
          }
          break;
        case 4: // Down
          if( !IsOverlap(&newMatRot,xPos,yPos-1,zPos) ) {
            yPos--;
            InitTranslate();
          }
          break;
        case 5: // Left,Up
          if( !IsOverlap(&newMatRot,xPos+1,yPos+1,zPos) ) {
            yPos++;xPos++;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos+1,yPos,zPos) ) {
            xPos++;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos,yPos+1,zPos) ) {
            yPos++;
            InitTranslate();
          }
          break;
        case 6: // Right,Up
          if( !IsOverlap(&newMatRot,xPos-1,yPos+1,zPos) ) {
            yPos++;xPos--;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos-1,yPos,zPos) ) {
            xPos--;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos,yPos+1,zPos) ) {
            yPos++;
            InitTranslate();
          }
          break;
        case 7: // Left,Down
          if( !IsOverlap(&newMatRot,xPos+1,yPos-1,zPos) ) {
            yPos--;xPos++;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos+1,yPos,zPos) ) {
            xPos++;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos,yPos-1,zPos) ) {
            yPos--;
            InitTranslate();
          }
          break;
        case 8: // Right,Down
          if( !IsOverlap(&newMatRot,xPos-1,yPos-1,zPos) ) {
            yPos--;xPos--;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos-1,yPos,zPos) ) {
            xPos--;
            InitTranslate();
          } else if( !IsOverlap(&newMatRot,xPos,yPos-1,zPos) ) {
            yPos--;
            InitTranslate();
          }
          break;

     }

     return TRUE;

  } else {

     if( dropMode )
       lastKeyMode = TRUE;
     return FALSE;

  }

}

// ---------------------------------------------------------------------
void Game::StartSpark(BLOCKITEM *pos) {

  soundManager->PlayHit();    
  startSpark = curTime;

  // Compute spark location
  VERTEX org  = thePit.GetOrigin();
  float cSide = thePit.GetCubeSide();

  D3DXVECTOR3 v = D3DXVECTOR3( (float)((pos->x) + 0.5f) * cSide + org.x,
                               (float)((pos->y) + 0.5f) * cSide + org.y,
                               (float)((pos->z) + 0.5f) * cSide + org.z);
  D3DXVECTOR4 r;
  D3DXMATRIX m;
  D3DXMatrixMultiply(&m,&matProj,&matView);
  D3DXVec3Transform(&r,&v,&m);

  int x = (int)(((-r.x / r.w) + 1.0f) * (float)pitView.Width/2.0f)  + pitView.X;
  int y = (int)(((r.y / r.w) + 1.0f) * (float)pitView.Height/2.0f) + pitView.Y;
  float pz = (float)pos->z;
  float pd = (float)thePit.GetDepth();
  float sw = (float)spriteView.Width;
  int w = fround( sw*0.05f * ((1.0f - pz/pd)*0.33f + 0.66f) );
  spark.UpdateSprite( x-w , y-w , x+w , y+w );

}

// ---------------------------------------------------------------------

BOOL Game::StartRotate(int rType) {

  if( gameMode==GAME_PAUSED || gameMode==GAME_OVER ) return TRUE;

  if( (rotateMode<=0) && !dropMode && !redMode ) {

    rotateMode = rType;
    startRotateTime = curTime;

    // Compute new rotation matrix

    switch(rotateMode) {
     case 1:
       D3DXMatrixMultiply(&newMatRot,&matRot,&matRotOx);
       break;
     case 2:
       D3DXMatrixMultiply(&newMatRot,&matRot,&matRotOy);
       break;
     case 3:
       D3DXMatrixMultiply(&newMatRot,&matRot,&matRotOz);
       break;
     case 4:
       D3DXMatrixMultiply(&newMatRot,&matRot,&matRotNOx);
       break;
     case 5:
       D3DXMatrixMultiply(&newMatRot,&matRot,&matRotNOy);
       break;
     case 6:
       D3DXMatrixMultiply(&newMatRot,&matRot,&matRotNOz);
       break;
    }

    // Check overlap
    int tx,ty,tz;
    BLOCKITEM pos;
    if( IsOverlap(&newMatRot,xPos,yPos,zPos,&tx,&ty,&tz,&pos) ) {
      // Check if the rotation can be done by a combination with a 
      // translation

      if( IsOverlap(&newMatRot,xPos+tx,yPos+ty,zPos+tz) ) {
        // Abort rotation
        rotateMode = -rotateMode;
        newMatRot = matRot;
        StartSpark(&pos);
      } else {
        // Combine rotation and translation
        xPos += tx;
        yPos += ty;
        zPos += tz;
        InitTranslate();
      }
    }

    return TRUE;

  } else {

    if( dropMode )
      lastKeyMode = TRUE;
    return FALSE;

  }

}

// ---------------------------------------------------------------------

int  Game::GetBottom() {

  int max = 0;

  // Get polycube cubes and transform them
  BLOCKITEM cubes[MAX_CUBE];
  int nbCube;
  allPolyCube[pIdx].CopyCube(cubes,&nbCube);
  TransformCube(&newMatRot,cubes,nbCube,xPos,yPos,zPos);

  // Get the bottom pos
  for(int i=0;i<nbCube;i++) 
    if(cubes[i].z >= max) max=cubes[i].z;

  return max;

}

// ---------------------------------------------------------------------

BOOL Game::IsLower() {

    BOOL upper = FALSE;

    // Get polycube cubes and transform them
    BLOCKITEM cubes[MAX_CUBE];
    int nbCube;
    allPolyCube[pIdx].CopyCube(cubes,&nbCube);
    TransformCube(&newMatRot,cubes,nbCube,xPos,yPos,zPos);

    // Check if all polycubes are above the cursor
    for(int i=0;i<nbCube && !upper;i++) {
      upper = (cubes[i].z >= cursorPos);
      if(!upper) i++;
    }

    return !upper;

}

// ---------------------------------------------------------------------

BOOL Game::IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz) {

  return IsOverlap(matRot,tx,ty,tz,NULL,NULL,NULL,NULL);

}

// ---------------------------------------------------------------------

BOOL Game::IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz,int *ox,int *oy,int *oz) {

  return IsOverlap(matRot,tx,ty,tz,ox,oy,oz,NULL);

}

// ---------------------------------------------------------------------

BOOL Game::IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz,BLOCKITEM *pos) {

  return IsOverlap(matRot,tx,ty,tz,NULL,NULL,NULL,pos);

}

// ---------------------------------------------------------------------

BOOL Game::IsOverlap(D3DXMATRIX *matRot,int tx,int ty,int tz,int *ox,int *oy,int *oz,BLOCKITEM *pos) {

    BOOL Overlap = FALSE;
    int mox = 0;
    int moy = 0;
    int moz = 0;
    int lox = 0;
    int loy = 0;
    int loz = 0;
    if( pos ) memset(pos,0,sizeof(BLOCKITEM));
    
    // Get polycube cubes and transform them
    BLOCKITEM cubes[MAX_CUBE];
    int nbCube;
    allPolyCube[pIdx].CopyCube(cubes,&nbCube);
    TransformCube(matRot,cubes,nbCube,tx,ty,tz);

    // Check overlap between the polycube and the pit
    for(int i=0;i<nbCube;i++) {
      if( thePit.GetValue( cubes[i].x , cubes[i].y , cubes[i].z ) ) {
        if( pos ) {
          pos->x = cubes[i].x;
          pos->y = cubes[i].y;
          pos->z = cubes[i].z;
        }
        Overlap = TRUE;
      }
      thePit.GetOutOfBounds( cubes[i].x , cubes[i].y , cubes[i].z , &lox , &loy , &loz );
      if( abs(lox) > abs(mox) ) mox = lox;
      if( abs(loy) > abs(moy) ) moy = loy;
      if( abs(loz) > abs(moz) ) moz = loz;
    }

    if( ox ) *ox = mox;
    if( oy ) *oy = moy;
    if( oz ) *oz = moz;

    return Overlap;

}

//-----------------------------------------------------------------------------

void Game::TransformCube(D3DXMATRIX *matRot,BLOCKITEM *cubes,int nbCube,
                             int tx,int ty,int tz) {

   BLOCKITEM center = allPolyCube[pIdx].GetICenter();
   for(int i=0;i<nbCube;i++) {

     D3DXVECTOR3 v = D3DXVECTOR3( (float)(cubes[i].x - center.x) + 0.5f,
                                  (float)(cubes[i].y - center.y) + 0.5f,
                                  (float)(cubes[i].z - center.z) + 0.5f );
     D3DXVECTOR4 r;
     D3DXVec3Transform(&r,&v,matRot);
     cubes[i].x = fround( r.x - 0.5f ) + center.x + tx ;
     cubes[i].y = fround( r.y - 0.5f ) + center.y + ty ;
     cubes[i].z = fround( r.z - 0.5f ) + center.z + tz ;

   }

}

//-----------------------------------------------------------------------------

int Game::SelectPolyCube() {

	// Get a new polycube
  pIdx = possible[rand() % nbPossible];

  // Translate the polycube to the left
  if( allPolyCube[pIdx].GetNbCube()>0 ) {
    xPos += thePit.GetWidth() - allPolyCube[pIdx].GetWidth();
    vPos.x = (float)xPos;
  }

  // Check game over state
  if( IsOverlap(&newMatRot,xPos,yPos,zPos) ) {
    score.gameTime = curTime - startGameTime;
    gameMode = GAME_OVER;
    FullRepaint();
  }

  return pIdx;

}

//-----------------------------------------------------------------------------

void Game::HandleKey(BYTE *keys) {

  if( keys['P'] ) {
    if( gameMode==GAME_PLAYING ) {
      gameMode = GAME_PAUSED;
      startPauseTime = curTime;
      FullRepaint();
    } else if ( gameMode==GAME_PAUSED ) {
      gameMode = GAME_PLAYING;
      // Update time stamps
      startRotateTime    += (curTime - startPauseTime);
      startTranslateTime += (curTime - startPauseTime);
      startRedTime       += (curTime - startPauseTime);
      startStepTime      += (curTime - startPauseTime);
      startGameTime      += (curTime - startPauseTime);
      FullRepaint();
    }
    keys['P']=0;
  }

  if( keys[VK_RETURN] ) {
    if( gameMode==GAME_OVER ) {
      // Return to menu
      if( demoFlag || practiceFlag )
        exitValue = 2;
      else
        exitValue = 1;
    }
    keys[VK_RETURN]=0;
  }

  if( keys[VK_ESCAPE] ) {
    // Abort the Game and return to menu
    if( gameMode==GAME_PLAYING ) {
      score.gameTime = curTime - startGameTime;
      gameMode=GAME_OVER;
    }
    if( demoFlag || practiceFlag )
      exitValue = 2;
    else
      exitValue = 1;
    keys[VK_ESCAPE]=0;
  }

	// Help mode
	if( keys['H'] && practiceFlag) {

		int tx,ty,tz;

    tx = thePit.GetWidth() - allPolyCube[pIdx].GetWidth();
		ty = 0;
		tz = 0;
    botPlayer.GetMoves(&thePit,&(allPolyCube[pIdx]),tx,ty,tz,AIMoves,&nbAIMove);
    D3DXMatrixIdentity(&matAI);
    for(int i=0;i<nbAIMove;i++) {
      switch( AIMoves[i].rotate ) {
       case 1:
         D3DXMatrixMultiply(&matAI,&matAI,&matRotOx);
         break;
       case 2:
         D3DXMatrixMultiply(&matAI,&matAI,&matRotOy);
         break;
       case 3:
         D3DXMatrixMultiply(&matAI,&matAI,&matRotOz);
         break;
       case 4:
         D3DXMatrixMultiply(&matAI,&matAI,&matRotNOx);
         break;
       case 5:
         D3DXMatrixMultiply(&matAI,&matAI,&matRotNOy);
         break;
       case 6:
         D3DXMatrixMultiply(&matAI,&matAI,&matRotNOz);
         break;
      }
      tx += AIMoves[i].tx;
      ty += AIMoves[i].ty;
      tz += AIMoves[i].tz;
    }

		// Drop
    while( !IsOverlap(&matAI,tx,ty,tz+1) ) tz++;

    // Transform
    D3DXMATRIX matT1;
    D3DXMATRIX matT2;
    D3DXMATRIX matT3;
    float cSide = thePit.GetCubeSide();

    D3DXMatrixTranslation(&matT3,tx*cSide,ty*cSide,tz*cSide);
    D3DXVECTOR3 center = allPolyCube[pIdx].GetRCenter();
    D3DXMatrixTranslation(&matT1,-center.x,-center.y,-center.z);
    D3DXMatrixTranslation(&matT2,center.x,center.y,center.z);

    D3DXMatrixMultiply(&matAI,&matT1,&matAI);
    D3DXMatrixMultiply(&matAI,&matAI,&matT2);
    D3DXMatrixMultiply(&matAI,&matAI,&matT3);
		startShowAI = curTime;

    keys['H']=0;
	}

#ifdef _DEBUG
	if( keys['X'] && practiceFlag) {
		keys['X'] = 0;
	}
#endif

  if( keys[VK_UP] || keys[VK_NUMPAD8] ) {
    if( StartTranslate(3) ) {
      keys[VK_UP]=0;
      keys[VK_NUMPAD8]=0;
    }
  }

  if( keys[VK_DOWN] || keys[VK_NUMPAD2] ) {
    if( StartTranslate(4) ) {
      keys[VK_DOWN]=0;
      keys[VK_NUMPAD2]=0;
    }
  }

  if( keys[VK_LEFT] || keys[VK_NUMPAD4] ) {
    if( StartTranslate(1) ) {
      keys[VK_LEFT]=0;
      keys[VK_NUMPAD4]=0;
    }
  }

  if( keys[VK_RIGHT] || keys[VK_NUMPAD6] ) {
    if( StartTranslate(2) ) {
      keys[VK_RIGHT]=0;
      keys[VK_NUMPAD6]=0;
    }
  }

  if( keys[VK_NUMPAD7] || keys[VK_HOME]) {
    if( StartTranslate(5) ) {
      keys[VK_NUMPAD7]=0;
      keys[VK_HOME]=0;
    }
  }

  if( keys[VK_NUMPAD9] || keys[VK_PRIOR]) {
    if( StartTranslate(6) ) {
      keys[VK_NUMPAD9]=0;
      keys[VK_PRIOR]=0;
    }
  }

  if( keys[VK_NUMPAD1] || keys[VK_END]) {
    if( StartTranslate(7) ) {
      keys[VK_NUMPAD1]=0;
      keys[VK_END]=0;
    }
  }

  if( keys[VK_NUMPAD3] || keys[VK_NEXT]) {
    if( StartTranslate(8) ) {
      keys[VK_NUMPAD3]=0;
      keys[VK_NEXT]=0;
    }
  }

  if( keys[VK_SPACE] ) {
    StartDrop();
    keys[VK_SPACE]=0;
  }

  if(keys[setupManager->GetKRz2()]) {
    if( StartRotate(3) ) { // Oz
      keys[setupManager->GetKRz2()]=0;
    }
  }

  if(keys[setupManager->GetKRy2()]) {
    if( StartRotate(2) ) { // Oy
      keys[setupManager->GetKRy2()]=0;
    }
  }

  if(keys[setupManager->GetKRx2()]) {
    if( StartRotate(1) ) { // Ox
      keys[setupManager->GetKRx2()]=0;
    }
  }

  if(keys[setupManager->GetKRz1()]) {
    if( StartRotate(6) ) { // -Oz
      keys[setupManager->GetKRz1()]=0;
    }
  }

  if(keys[setupManager->GetKRy1()]) {
    if( StartRotate(5) ) { // -Oy
      keys[setupManager->GetKRy1()]=0;
    }
  }

  if(keys[setupManager->GetKRx1()]) {
    if( StartRotate(4) ) { // -Ox
      keys[setupManager->GetKRx1()]=0;
    }
  }

}

//-----------------------------------------------------------------------------

void Game::FullRepaint() {
  // 2 times for the 2 buffers
  fullRepaint = 2;
}
