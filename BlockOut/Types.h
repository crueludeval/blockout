/*
 	File:        Types.h 
  Description: Various basic types definitions
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#define STRICT
#include <D3DX8.h>

#ifndef TYPESH
#define TYPESH

//-----------------------------------------------------------------------------
// Global definitions
//-----------------------------------------------------------------------------

#define PI                 3.1415926535f
#define STARTZ             0.87f
#define FAR_DISTANCE       10.0f
#define MAX_CUBE           25
#define NB_POLYCUBE        41

// Vertex format
#define VERTEX_FORMAT      (D3DFVF_XYZ)
#define FACEVERTEX_FORMAT  (D3DFVF_XYZ | D3DFVF_NORMAL)
#define TFACEVERTEX_FORMAT  (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

// Max pit dimension
#define MAX_PITWIDTH       7
#define MAX_PITHEIGHT      7
#define MAX_PITDEPTH       18

// Min pit dimension
#define MIN_PITWIDTH       3
#define MIN_PITHEIGHT      3
#define MIN_PITDEPTH       6

// Block set
#define BLOCKSET_FLAT      0
#define BLOCKSET_BASIC     1
#define BLOCKSET_EXTENDED  2

// Animation speed
#define ASPEED_SLOW        0
#define ASPEED_FAST        10

// Face transparency
#define FTRANS_MIN         0 
#define FTRANS_MAX         10

// Game state
#define GAME_PLAYING       1
#define GAME_PAUSED        2
#define GAME_OVER          3
#define GAME_DEMO          4

// Screen resoltion
#define RES_640x480        0
#define RES_800x600        1
#define RES_1024x768       2
#define RES_1280x1024      3

// Game style
#define STYLE_CLASSIC      0
#define STYLE_MARBLE       1
#define STYLE_ARCADE       2

// Game sound
#define SOUND_BLOCKOUT2    0
#define SOUND_BLOCKOUT     1

// Frame limiter
#define FR_NOLIMIT    0
#define FR_LIMIT50    1
#define FR_LIMIT60    2
#define FR_LIMIT75    3
#define FR_LIMIT100   4

// Antialias level
#define AA_NONE      0
#define AA_LOW       1
#define AA_MEDIUM    2
#define AA_HIGH      3

// Line width
#define LINEW_MIN    0 
#define LINEW_MAX    10

//-----------------------------------------------------------------------------
// Structure definitions
//-----------------------------------------------------------------------------

typedef struct  {

  float x;
  float y;
  float z;

} VERTEX;

typedef struct  {

  float x;
  float y;
  float z;
  float nx;
  float ny;
  float nz;

} FACEVERTEX;

typedef struct  {

  float x;
  float y;
  float z;
  float nx;
  float ny;
  float nz;
  float tu;
  float tv;

} TFACEVERTEX;

typedef struct {

  int x;
  int y;
  int z;

} BLOCKITEM;

typedef struct {

	BLOCKITEM p1;
	BLOCKITEM p2;
	int orientation;

} EDGE;

typedef struct {

	VERTEX p;
	VERTEX o;

} CORNER;

typedef struct {

	int r0;
	int r1;
	int r2;

} ORIENTATION;

typedef struct SCORERECLINK {

  int   setupId;
  int   score;
  int   nbCube;
  int   nbLine1;
  int   nbLine2;
  int   nbLine3;
  int   nbLine4;
  int   nbLine5;
  int   startLevel;
  int   date;
  char  name[11];
  BYTE  emptyPit;
  int   scoreId;
  float gameTime;

  SCORERECLINK *next;

} SCOREREC;

typedef struct {

  char name[11];
  int  rank;
  int  highScore;

} PLAYER_INFO;


typedef struct {

  int rotate;
  int tx;
  int ty;
  int tz;

} AI_MOVE;

//-----------------------------------------------------------------------------
// Util functions
//-----------------------------------------------------------------------------

extern VERTEX v(float x,float y,float z);
extern FACEVERTEX v(float x,float y,float z,float nx,float ny,float nz);
extern TFACEVERTEX v(float x,float y,float z,float nx,float ny,float nz,float tu,float tv);
extern char *GetDDErrorMsg(HRESULT code);
extern int fround(float x);
extern void SetPix(D3DLOCKED_RECT *d3dlr,D3DFORMAT format,int x,int y,DWORD color);
extern char *FormatTime(float seconds);
extern char *FormatDate(int time);
extern char *FormatDateShort(int time);
extern HRESULT CreateColorTexture(LPDIRECT3DDEVICE8 pd3dDevice,int width,int height,D3DFORMAT format,DWORD color,BYTE alpha,LPDIRECT3DTEXTURE8 *hmap);
extern HRESULT CreateTexture(LPDIRECT3DDEVICE8 pd3dDevice,int width,int height,D3DFORMAT format,char *imgName,LPDIRECT3DTEXTURE8 *hmap);
extern char GetChar(BYTE *keys,BOOL editMaj);

#endif /* TYPESH */