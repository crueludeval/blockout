/*
 	File:        Sprites.h
  Description: Handles game sprites and fonts
  Program:     BlockOut
  Author:      Jean-Luc PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

#define STRICT
#include <D3DX8.h>
#include <D3DObject.h>
#include "Types.h"

class Sprites {

  public:
    Sprites();

    // Create Sprites device objects
    HRESULT Create(LPDIRECT3DDEVICE8 pd3dDevice,DWORD width,DWORD height);

    // Release device objects
    void InvalidateDeviceObjects();

    // Render score,level and cube
    void RenderScore(LPDIRECT3DDEVICE8 pd3dDevice,DWORD score,DWORD level,DWORD cubes);

    // Render High score, pit dimension, game mode and block set
    void RenderInfo(LPDIRECT3DDEVICE8 pd3dDevice,DWORD highScore,int x,int y,int z,int blockSet);

    // Render "Game Over" and "PAUSED"
    void RenderGameMode(LPDIRECT3DDEVICE8 pd3dDevice,int mode);

    // Render "Replay"
    void RenderReplay(LPDIRECT3DDEVICE8 pd3dDevice);

    // Render "Demo"
    void RenderDemo(LPDIRECT3DDEVICE8 pd3dDevice);

    // Render "Practice"
    void RenderPractice(LPDIRECT3DDEVICE8 pd3dDevice);

    // Render "On Line"
    void RenderOnLine(LPDIRECT3DDEVICE8 pd3dDevice);

  private:

    void RenderNumbers(LPDIRECT3DDEVICE8 pd3dDevice,int x,int y,char *strMumber);
    void RenderLevel(LPDIRECT3DDEVICE8 pd3dDevice,int level);
    void RenderBlockSet(LPDIRECT3DDEVICE8 pd3dDevice,int blockSet);
    void RenderMode(LPDIRECT3DDEVICE8 pd3dDevice,int mode);
    int GetNumberWidth(char number);

    D3DObject baseSprite;
    D3DObject gameOverSprite;

    // Coordinates
    int xScore;
    int yScore;
    int xCube;
    int yCube;
    int xhScore;
    int yhScore;
    int xPit;
    int yPit;
    int xBlock;
    int yBlock;
    int wScore;
    int hScore;

    int xLevel;
    int yLevel;
    int wLevel;
    int hLevel;

    int xReplay;
    int yReplay;
    int wReplay;
    int hReplay;

    int xOnline;
    int yOnline;
    int wOnline;
    int hOnline;

    int xGOver;
    int yGOver;
    int wGOver;
    int hGOver;

    int numberWidth[10];

};